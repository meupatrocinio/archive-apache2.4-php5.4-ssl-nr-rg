meupatrocinio/apache2.4-php5.4-ssl-nr-rg
==========


This Docker container implements a LAP stack, as well as some popular PHP modules and a Postfix service to allow sending emails through PHP [mail()](http://php.net/manual/en/function.mail.php) function.

If you are looking for a modern, developer friendly container for your latest app, maybe [fauria/lamp](https://hub.docker.com/r/fauria/lamp) suits your needs better.

Includes the following components:

 * Centos 7 base image.
 * Apache HTTP Server 2.4
 * SSL Suport
 * Postfix 2.10
 * Railgun
 * New Relic
 * PHP 5.4
 * PHP modules
 	* php-common
	* php-dba
	* php-gd
	* php-intl
	* php-ldap
	* php-mbstring
	* php-mysqlnd
	* php-odbc
	* php-pdo
	* php-pecl-memcache
	* php-pgsql
	* php-pspell
	* php-recode
	* php-snmp
	* php-soap
	* php-xml
	* php-xmlrpc



Installation from [Docker registry hub](https://registry.hub.docker.com/u/meupatrocinio/apache2.4-php5.4-ssl-nr-rg).
----

You can download the image using the following command:

```bash
docker pull meupatrocinio/apache2.4-php5.4-ssl-nr-rg
```

Environment variables
----

This image uses environment variables to allow the configuration of some parameteres at run time:

* Variable name: `LOG_STDOUT`
* Default value: Empty string.
* Accepted values: Any string to enable, empty string or not defined to disable.
* Description: Output Apache access log through STDOUT, so that it can be accessed through the [container logs](https://docs.docker.com/reference/commandline/logs/).

----

* Variable name: `LOG_STDERR`
* Default value: Empty string.
* Accepted values: Any string to enable, empty string or not defined to disable.
* Description: Output Apache error log through STDERR, so that it can be accessed through the [container logs](https://docs.docker.com/reference/commandline/logs/).

----

* Variable name: `LOG_LEVEL`
* Default value: warn
* Accepted values: debug, info, notice, warn, error, crit, alert, emerg
* Description: Value for Apache's [LogLevel directive](http://httpd.apache.org/docs/2.4/en/mod/core.html#loglevel).

----

* Variable name: `ALLOW_OVERRIDE`
* Default value: All
* All, None
* Accepted values: Value for Apache's [AllowOverride directive](http://httpd.apache.org/docs/2.4/en/mod/core.html#allowoverride).
* Description: Used to enable (`All`) or disable (`None`) the usage of an `.htaccess` file.

----

* Variable name: `DATE_TIMEZONE`
* Default value: UTC
* Accepted values: Any of PHP's [supported timezones](http://php.net/manual/en/timezones.php)
* Description: Set php.ini default date.timezone directive.

----

* Variable name: `ALLOW_OVERRIDE`
* Default value: All
* All, None
* Accepted values: Value for Apache's [AllowOverride directive](http://httpd.apache.org/docs/2.4/en/mod/core.html#allowoverride).
* Description: Used to enable (`All`) or disable (`None`) the usage of an `.htaccess` file.

----

* Variable name: `DATE_TIMEZONE`
* Default value: UTC
* Accepted values: Any of PHP's [supported timezones](http://php.net/manual/en/timezones.php)
* Description: Set php.ini default date.timezone directive.

----

* Variable name: `RAILGUN_MEMCACHED_PORT`
* Default value: 11211 
* All, None
* Accepted values: Value for Mencached Port.

----

* Variable name: `RAILGUN_MEMCACHED_SERVER`
* Accepted values: memcached container link or other server.

----

* Variable name: `NR_APP_NAME`
* Default value: PHP aplication

* Accepted values: Value for New Relic Aplication Name.

----

* Variable name: `NR_INSTALL_KEY`
* Accepted values: Your NewRelic install key.

----

* Variable name 'RAILGUN_TOKEN'
* Accepted values: Your Railgun token 


