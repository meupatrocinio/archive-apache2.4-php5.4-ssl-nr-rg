#!/bin/env bash

[ -z "$RAILGUN_TOKEN" ] && { echo "Need to set TOKEN"; exit 1; }

RAILGUN_HOST=$(curl icanhazip.com)

echo "Updating Config file"
sed -i "s/YOUR_TOKEN_HERE/${RAILGUN_TOKEN}/" /etc/railgun/railgun.conf
sed -i "s/YOUR_PUBLIC_IP_OR_HOSTNAME/${RAILGUN_HOST}/" /etc/railgun/railgun.conf
sed -i "s/127.0.0.1:11211/${RAILGUN_MEMCACHED_SERVER:-memcache}:${RAILGUN_MEMCACHED_PORT:-11211}/" /etc/railgun/railgun.conf
sed -i "s/log.level = 0/log.level = ${RAILGUN_LOG_LEVEL:-0}/" /etc/railgun/railgun.conf

echo "Create log file if not present"
[ ! -f /var/log/railgun/panic.log ] && { touch /var/log/railgun/panic.log; }

echo "Starting Railgun"
RAILGUN_PATH=$(/usr/bin/which rg-listener)
$RAILGUN_PATH -config "/etc/railgun/railgun.conf" &
